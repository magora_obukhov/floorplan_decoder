//
//  ShareViewController.swift
//  Sharing extension
//
//  Created by Denis Obukhov on 29.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

import UIKit
import Social

class ShareViewController: UIViewController {

    @IBOutlet var loadingView: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var totalWidthLabel: UILabel!
    @IBOutlet var totalHeightLabel: UILabel!
    @IBOutlet var labelsCountLabel: UILabel!
    
    let parser = FloorplanParser()
    let reporter = FloorPlanReporter()
    var floorPlan: FPFloorplan?
    var labels: [FPLabel] = []
    
    private let cellID = "FloorLabelTableViewCell"
    private let reporterStrategy = ReporterStrategy.filterNotDoubles

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: self.cellID, bundle: nil), forCellReuseIdentifier: self.cellID)
        self.setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.handleInputData()
    }

    private func setupViews() {
        self.imageView.layer.borderColor = UIColor.lightGray.cgColor
        self.imageView.layer.borderWidth = 1
    }
    
    private func handleInputData() {
        guard let item = self.extensionContext?.inputItems.first as? NSExtensionItem else { return }
        guard let provider = item.attachments?.first else { return }
        
        if provider.hasItemConformingToTypeIdentifier(Constants.floorplanFileTypeID) {
            self.animateLoadingView(isHidden: false)
            provider.loadItem(forTypeIdentifier: Constants.floorplanFileTypeID,
                              options: nil) { (secureCoding, error) in
                                if let error = error as? LocalizedError {
                                    self.show(error: error)
                                    return
                                }
                                guard
                                    let fileURL = secureCoding as? URL,
                                    fileURL.isFileURL
                                    else { return }
                                self.parser.parse(fileURL: fileURL) { (floorPlan, error) in
                                    if let error = error as? LocalizedError {
                                        self.show(error: error)
                                        return
                                    }
                                    self.floorPlan = floorPlan
                                    self.labels = floorPlan?.floors
                                        .flatMap { $0.labels }
                                        .filter { self.reporterStrategy.evaluate(on: $0.text) }
                                        ?? []
                                    var thumbnailImage: UIImage?
                                    if let thumbnail = floorPlan?.thumbnail {
                                        thumbnailImage = UIImage(data: thumbnail)
                                    }
                                    DispatchQueue.main.async {
                                        self.animateLoadingView(isHidden: true)
                                        self.imageView.image = thumbnailImage
                                        self.totalWidthLabel.text = String(floorPlan?.width ?? 0)
                                        self.totalHeightLabel.text = String(floorPlan?.length ?? 0)
                                        self.labelsCountLabel.text = String(self.labels.count)
                                        self.tableView.reloadData()
                                    }
                                }
            }
        } else {
            self.show(error: FloorplanParserError.cantReadFile)
        }
    }
    
    private func animateLoadingView(isHidden: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.loadingView.alpha = isHidden ? 0 : 0.3
        }
    }
    
    @IBAction func shareButtonAction(_ sender: Any) {
        guard let floorPlan = self.floorPlan else { return }
        self.animateLoadingView(isHidden: false)
        self.reporter.composeReport(floorPlan: floorPlan, filterStrategy: self.reporterStrategy) { report in
            DispatchQueue.main.async {
                var activityItems: [Any] = [report]
                if let image = self.imageView.image {
                    activityItems.append(image)
                }
                let activityViewController = UIActivityViewController(
                    activityItems: activityItems, applicationActivities: nil)
                
                self.animateLoadingView(isHidden: true)
                self.present(activityViewController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        self.extensionContext?.completeRequest(returningItems: nil, completionHandler: nil)
    }
}

extension ShareViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.labels.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellID) as? FloorLabelTableViewCell else {
            return UITableViewCell()
        }
        let floorLabel = self.labels[indexPath.row]
        cell.labelText.text = floorLabel.text
        
        [(cell.xCoordinate, floorLabel.positionX),
         (cell.yCoordinate, floorLabel.positionY),
         (cell.widthLabel, floorLabel.width),
         (cell.heightLabel, floorLabel.height)]
            .forEach { label, value in
                label?.text = String(value)
        }
        return cell
    }
}

extension FloorplanParserError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .cantReadFile:
            return "Can't read the file"
        case .invalidFormat:
            return "Invalid file format"
        }
    }
}

extension ShareViewController {
    private func show(error: LocalizedError) {
        let vc = UIAlertController(title: "Error",
                                   message: error.localizedDescription,
                                   preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { action in
            self.extensionContext?.cancelRequest(withError: FloorplanParserError.cantReadFile)
        }
        vc.addAction(okAction)
        
        self.present(vc, animated: true, completion: nil)
    }
}
