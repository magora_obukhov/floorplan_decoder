//
//  FloorLabelTableViewCell.swift
//  Parser
//
//  Created by Denis Obukhov on 02.03.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

import UIKit

class FloorLabelTableViewCell: UITableViewCell {

    @IBOutlet var labelText: UILabel!
    @IBOutlet var xCoordinate: UILabel!
    @IBOutlet var yCoordinate: UILabel!
    @IBOutlet var widthLabel: UILabel!
    @IBOutlet var heightLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.labelText.text = nil
        self.xCoordinate.text = nil
        self.yCoordinate.text = nil
        self.widthLabel.text = nil
        self.heightLabel.text = nil
    }

}
