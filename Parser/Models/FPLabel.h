//
//  FPLabel.h
//  Parser
//
//  Created by Denis Obukhov on 28.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

#import "FPContainer.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPLabel : FPContainer

@property (assign, nonatomic) double positionX;
@property (assign, nonatomic) double positionY;
@property (assign, nonatomic) double height;
@property (assign, nonatomic) double width;
@property (strong, nonatomic, nullable) NSString *text;

@end

NS_ASSUME_NONNULL_END
