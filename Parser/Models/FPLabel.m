//
//  FPLabel.m
//  Parser
//
//  Created by Denis Obukhov on 28.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

#import "FPLabel.h"

@implementation FPLabel

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.positionX = [coder decodeDoubleForKey:@"positionX"];
        self.positionY = [coder decodeDoubleForKey:@"positionY"];
        self.text = [coder decodeObjectForKey:@"text"];
        self.width = [coder decodeDoubleForKey:@"width"];
        self.height = [coder decodeDoubleForKey:@"height"];
    }
    return self;
}

@end
