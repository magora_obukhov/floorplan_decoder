//
//  FPFloorplan.m
//  Parser
//
//  Created by Denis Obukhov on 28.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

#import "FPFloorplan.h"

@implementation FPFloorplan

- (instancetype)initWithCoder:(NSCoder *)coder {
    self.width = [coder decodeDoubleForKey:@"width"];
    self.length = [coder decodeDoubleForKey:@"length"];
    self.floors = [coder decodeObjectForKey:@"floors"];
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)coder {
    
}

@end
