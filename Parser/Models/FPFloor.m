//
//  FPFloor.m
//  Parser
//
//  Created by Denis Obukhov on 28.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

#import "FPFloor.h"

@implementation FPFloor


- (instancetype)initWithCoder:(NSCoder *)coder {
    NSSet<FPLabel *> *labelsSet = [coder decodeObjectForKey:@"labels"];
    self.labels = labelsSet.allObjects;
    return self;
}

- (void)encodeWithCoder:(nonnull NSCoder *)coder {
    
}

@end
