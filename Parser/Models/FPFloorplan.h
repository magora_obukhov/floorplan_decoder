//
//  FPFloorplan.h
//  Parser
//
//  Created by Denis Obukhov on 28.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPFloor.h"


NS_ASSUME_NONNULL_BEGIN

@interface FPFloorplan : NSObject <NSCoding>

@property (assign, nonatomic) double length;
@property (assign, nonatomic) double width;
@property (strong, nonatomic) NSArray<FPFloor *> *floors;
@property (strong, nonatomic, nullable) NSData *thumbnail;
@end

NS_ASSUME_NONNULL_END
