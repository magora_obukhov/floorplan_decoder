//
//  FPFloorObject.h
//  Parser
//
//  Created by Denis Obukhov on 28.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FPFloorObject : NSObject <NSCoding>

- (void)encodeWithCoder:(nonnull NSCoder *)coder;
- (nullable instancetype)initWithCoder:(nonnull NSCoder *)coder;

@end

NS_ASSUME_NONNULL_END
