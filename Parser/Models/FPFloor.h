//
//  FPFloor.h
//  Parser
//
//  Created by Denis Obukhov on 28.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FPLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPFloor : NSObject <NSCoding>

@property (strong, nonatomic) NSArray<FPLabel *> *labels;

@end

NS_ASSUME_NONNULL_END
