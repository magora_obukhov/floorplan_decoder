//
//  FPContainer.h
//  Parser
//
//  Created by Denis Obukhov on 28.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

#import "FPFloorObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface FPContainer : FPFloorObject 

@end

NS_ASSUME_NONNULL_END
