//
//  AppDelegate.swift
//  Parser
//
//  Created by Denis Obukhov on 28.02.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

import UIKit


let parser = FloorplanParser()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        
        /*
        let path = Bundle.main.path(forResource: "1.floorplan", ofType: nil)!
        let fileURL = URL(fileURLWithPath: path)
        parser.parse(fileURL: fileURL) { (floorPlan, error) in
            guard let floorPlan = floorPlan else {
                print("Error: \(error!)")
                return
            }
            print("Floor plan")
            print("   width: \(floorPlan.width), lenght: \(floorPlan.length)")
            print("Labels:")
            for floor in floorPlan.floors {
                for (index, label) in floor.labels.enumerated() {
                    print("   [\(index)] text : \(label.text), "
                        + "x: \(label.positionX), "
                        + "y: \(label.positionY), "
                        + "width: \(label.width), "
                        + "height: \(label.height)"
                    )
                }
            }
        }
*/
        return true
    }
}
