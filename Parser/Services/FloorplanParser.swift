import Foundation

enum FloorplanParserError: Error {
    case invalidFormat
    case cantReadFile
}


class FloorplanParser {    
    private class RootFloorPlanClass: NSObject, Codable {
        var GUID: String?
        var floorplanData: Data
        var needsCalibration: Bool?
        var documentVersion: String?
        var thumbnail: Data?
        
        enum CodingKeys: String, CodingKey {
            case GUID
            case floorplanData = "FloorplanNSData"
            case needsCalibration = "NeedsCalibration"
            case documentVersion = "DocumentVersion"
            case thumbnail = "Thumbnail"
        }
    }
    
    typealias ParseCompletion = (FPFloorplan?, Error?) -> Void

    private let queue = DispatchQueue.global(qos: .userInitiated)
    
    func parse(fileURL: URL, completion: @escaping ParseCompletion) {
        self.queue.async { [weak self] in
            guard
                let data = try? Data(contentsOf: fileURL)
            else {
                completion(nil, FloorplanParserError.cantReadFile)
                return
            }
            self?.parse(data: data, completion: completion)
        }
    }
    
    func parse(data: Data, completion: @escaping ParseCompletion) {
        self.queue.async { [weak self] in
            do {
                let result = try self?.deserialize(data: data)
                completion(result, nil)
            } catch {
                completion(nil, error)
            }
        }
    }
    
    private func deserialize(data: Data) throws -> FPFloorplan? {
        let decoder = PropertyListDecoder()
        let root = try! decoder.decode(RootFloorPlanClass.self, from: data)
        do {
            guard
                let floorPlan = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(root.floorplanData) as? FPFloorplan
            else {
                throw FloorplanParserError.invalidFormat
            }
            floorPlan.thumbnail = root.thumbnail
            return floorPlan
        } catch {
            print("Couldn't read file.\(error)")
            throw FloorplanParserError.cantReadFile
        }
    }
}
