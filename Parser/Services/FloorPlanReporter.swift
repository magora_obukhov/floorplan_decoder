import Foundation

enum ReporterStrategy {
    typealias Predicate = (String?) -> Bool
    
    case filterNotDoubles
    case custom(Predicate)
    
    func evaluate(on stringToCheck: String?) -> Bool {
        switch self {
        case .filterNotDoubles:
            guard let stringToCheck = stringToCheck else { return false }
            var trimmedValue = stringToCheck.trimmingCharacters(in: .whitespacesAndNewlines)
            trimmedValue = trimmedValue.trimmingCharacters(in: CharacterSet(charactersIn: "+-"))
            return Double(trimmedValue) != nil
            
        case .custom(let strategy):
            return strategy(stringToCheck)
        }
    }
}

class FloorPlanReporter {
    func composeReport(floorPlan: FPFloorplan, filterStrategy: ReporterStrategy? = .filterNotDoubles, completion: @escaping (String) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var result: String = "Floor plan report:\n\n"
            result += "Total height: \(floorPlan.length)\n"
            result += "Total width: \(floorPlan.width)\n\n"
            result += "Format: \"value\" x y width height\n\n"
            for floor in floorPlan.floors {
                for label in floor.labels {
                    if let filterStrategy = filterStrategy, !filterStrategy.evaluate(on: label.text) {
                        continue
                    }
                    result += "\"\((label.text ?? ""))\""
                    result += " \(label.positionX) \(label.positionY)"
                    result += " \(label.width) \(label.height)"
                    result += "\n"
                }
            }
            completion(result)
        }
    }
}
