//
//  Constants.swift
//  Parser
//
//  Created by Denis Obukhov on 02.03.2020.
//  Copyright © 2020 Denis Obukhov. All rights reserved.
//

import Foundation

struct Constants {
    static let floorplanFileTypeID = "com.emilchoski.floorplan"
}
